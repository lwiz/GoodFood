package us.lukemontalvo.GoodFood;

import java.util.Collection;
import java.util.UUID;
import java.util.logging.Level;

import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GoodFoodCommand implements CommandExecutor {
	private GoodFood gf = null;

	public GoodFoodCommand(GoodFood _gf) {
		super();
		gf = _gf;
	}

	public void printHelp(CommandSender sender) {
		GoodFood.senderLog(sender, Level.WARNING, "Usage: /goodfood @help");
		GoodFood.senderLog(sender, Level.WARNING, "    or /goodfood @reload");
		GoodFood.senderLog(sender, Level.WARNING, "    or /goodfood @debug [hunger] [saturation] [exhaustion]");
		GoodFood.senderLog(sender, Level.WARNING, "    or /goodfood @on");
		GoodFood.senderLog(sender, Level.WARNING, "    or /goodfood @off");
		GoodFood.senderLog(sender, Level.WARNING, "    or /goodfood player_name");
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (sender instanceof Player) {
			if (args.length == 0) {
				if (!sender.hasPermission("GoodFood.toggle_self")) {
					GoodFood.senderLog(sender, Level.WARNING, "No permission to toggle GoodFood on yourself!");
					return true;
				}

				togglePlayer(sender.getName());
				GoodFood.senderLog(sender, Level.INFO, String.format("GoodFood toggled %s", this.gf.getPlayerEnabled((Player) sender) ? "on" : "off"));

				return true;
			}
		}

		if (args.length == 0) {
			printHelp(sender);
			return true;
		}

		if (args[0].equalsIgnoreCase("@help")) {
			printHelp(sender);
		} else if (args[0].equalsIgnoreCase("@reload")) {
			if (!sender.hasPermission("GoodFood.reload")) {
				GoodFood.senderLog(sender, Level.WARNING, "No permission to reload the GoodFood config!");
				return true;
			}

			this.gf.reloadConfig();
			GoodFood.senderLog(sender, Level.INFO, "GoodFood config reloaded");
		} else if (args[0].equalsIgnoreCase("@debug")) {
			if (!sender.hasPermission("GoodFood.debug")) {
				GoodFood.senderLog(sender, Level.WARNING, "No permission to debug GoodFood!");
				return true;
			}
			
			Player p = (Player) sender;
			
			if (args.length > 1) {
				p.setFoodLevel(Integer.parseInt(args[1]));
			}
			if (args.length > 2) {
				p.setSaturation((float) Double.parseDouble(args[2]));
			}
			if (args.length > 3) {
				p.setExhaustion((float) Double.parseDouble(args[3]));
			}
			
			GoodFood.senderLog(sender, Level.INFO, String.format("hunger: %d, saturation: %f, exhaustion: %f", p.getFoodLevel(), p.getSaturation(), p.getExhaustion()));
		} else {
			if (!sender.hasPermission("GoodFood.toggle_any")) {
				GoodFood.senderLog(sender, Level.WARNING, "No permission to toggle GoodFood on others!");
				return true;
			}

			if (args[0].equalsIgnoreCase("@on")) {
				toggleAll(true);
				GoodFood.senderLog(sender, Level.INFO, "GoodFood toggled on for all players");
			} else if (args[0].equalsIgnoreCase("@off")) {
				toggleAll(false);
				GoodFood.senderLog(sender, Level.INFO, "GoodFood toggled off for all players");
			} else {
				boolean is_enabled = togglePlayer(args[0]);
				GoodFood.senderLog(sender, Level.INFO, String.format("GoodFood toggled %s for %s", is_enabled, args[0]));
			}
		}

		return true;
	}

	private void toggleAll(boolean is_enabled) {
		Collection<? extends Player> onplayers = this.gf.getServer().getOnlinePlayers();
		OfflinePlayer[] offplayers = this.gf.getServer().getOfflinePlayers();

		for (Player p : onplayers) {
			p.setSaturation(5.0f);
			this.gf.config.set(String.format("players.%s.is_enabled", p.getUniqueId()), is_enabled);
		}
		for (OfflinePlayer offp : offplayers) {
			this.gf.config.set(String.format("players.%s.is_enabled", offp.getUniqueId()), is_enabled);
		}

		this.gf.saveConfig();
	}
	private boolean togglePlayer(String player_name) {
		Player p = this.gf.getServer().getPlayer(player_name);
		UUID u;
		if (p == null) {
			@SuppressWarnings("deprecation")
			OfflinePlayer offp = this.gf.getServer().getOfflinePlayer(player_name);
			if (!offp.hasPlayedBefore()) {
				return false;
			}
			u = offp.getUniqueId();
		} else {
			p.setSaturation(5.0f);
			u = p.getUniqueId();
		}

		this.gf.config.set(String.format("players.%s.is_enabled", u), !this.gf.getPlayerEnabled(u));
		this.gf.saveConfig();

		return this.gf.getPlayerEnabled(u);
	}
}
